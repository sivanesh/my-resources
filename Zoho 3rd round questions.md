# Round 3
## Question 1
Given N and M generate an N X M matrix and with values 1, -1.
*(A method for generating random number is provided)*
### Sample i/o
Enter n and m
3
4
1	-1  1	1

1	 1	 -1   -1

-1 -1  -1  -1

## Question 2
A **sum** value is given. Every column is horizontally traversed and summed up.
If summed value = **sum** then make the elements 0.
*Condition: sum should not be 0 or 1 or -1.*
 ### Sample i/o
Enter n and m
3
4
Enter sum
2
Random matrix generated is 
1	-1  1	1

1	 1	 -1   -1

-1 -1  -1  -1

Answer is 
1	-1  0	0

0	0	 -1   -1

-1 -1  -1  -1

## Question 3 
After last program, Check the sum as before with vertical.
First priority is for horizontal.
 ### Sample i/o
Enter n and m
3
4
Enter sum
2
Random matrix generated is 
1	-1  1	1

1	 1	 -1   1

-1 -1  -1  1

Answer is 
1	-1  0	0

0	0	 -1   0

-1 -1  -1  0


## Question 4 
Same with diagonals